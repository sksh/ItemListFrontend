/**
 * Created by skashchin on 17.03.2016.
 */
'use strict';

itemsApp.factory('categoryService', ['$resource',
    function ($resource) {
        return $resource(itemsApp.SERVER_URL + '/api/category', {}, {
            listAllCategories: {method: 'GET', isArray: true}
        });
    }]);