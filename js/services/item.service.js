/**
 * Created by Sviat on 3/15/16.
 */

'use strict';

itemsApp.factory('itemService', ['$resource', function ($resource) {
    return $resource(itemsApp.SERVER_URL + "/api/item/:id", {id: '@id'},
        {
            'saveItem': {method: 'POST'},
            'listAllItems': {method: 'GET', isArray: true},
            'getById': {method: 'GET', params: {id: '@id'}},
            'update': {method: 'PUT'},
            'deleteItem': {method: 'DELETE', params: {id: '@id'}}
        }
    );
}]);