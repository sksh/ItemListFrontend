/**
 * Created by skashchin on 31.03.2016.
 */
'use strict';

itemsApp.directive('itemTime', function() {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/ItemCreatedTimeDirective.html',
        scope: {
            created: "=",
            updated: "="
        }
    }
});