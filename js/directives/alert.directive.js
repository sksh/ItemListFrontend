/**
 * Created by skashchin on 04.04.2016.
 */

itemsApp.directive('alertBox', function() {
    return {
        templateUrl: 'templates/directives/AlertDirective.html'
    }
});