/**
 * Created by skashchin on 31.03.2016.
 */

'use strict';

itemsApp.directive('itemDetails', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/directives/ItemDetails.html',
            controller: 'ItemController',
            scope: {
                item: "="
            }
        }
    }
);