/**
 * Created by skashchin on 05.04.2016.
 */

'use strict';

itemsApp.directive('itemRow', function () {
    return {
        restrict: 'A',
        templateUrl: 'templates/directives/ItemRow.html',
        replace: false,
        scope: {
            item: '=',
            showItemDetails: '&',
            deleteItem: '&'
        },
        link: function (scope, elem, attrs) {
            elem.bind('click', function() {
                console.log(scope.item);
            });
        }
    }
});