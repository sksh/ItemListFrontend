/**
 * Created by skashchin on 31.03.2016.
 */
'use strict';

itemsApp.directive('itemCategories', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/ItemCategoriesDirective.html',
        scope: {
            add: "&",
            remove: "&",
            categories: "=",
            available: "="
        }
    }
});