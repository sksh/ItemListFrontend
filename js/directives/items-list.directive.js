/**
 * Created by skashchin on 31.03.2016.
 */
'use strict';

itemsApp.directive('itemList', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/ItemListDirective.html',
        replace: true,
        controller: 'ItemsListController',
        scope: {
            items: "="
        }
    }
});