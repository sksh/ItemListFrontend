/**
 * Created by Sviat on 3/15/16.
 */

'use strict';

var itemsApp = angular.module('iApp', ['ngResource', 'ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/list', {
                templateUrl: 'templates/ItemList.html',
                controller: 'ItemsListController'
            })

            .when('/details/:itemId', {
                templateUrl: 'templates/ItemDetails.html',
                controller: 'ItemController'
            })

            .when('/create', {
                shouldCreate: true,
                templateUrl: 'templates/ItemDetails.html',
                controller: 'ItemController'
            })

            .when('/categories', {
                templateUrl: 'templates/Categories.html',
                controller: 'CategoryController as vm'
            })

            .when('/directive', {
                templateUrl: 'templates/directives/SampleDirective.html',
                //controller: 'CategoryController'
            })

            .otherwise({redirectTo: '/list'});
    });

itemsApp.SERVER_URL = 'http://localhost:8080';