/**
 * Created by skashchin on 17.03.2016.
 */
'use strict';

itemsApp.controller('CategoryController', ['categoryService', '$log',
    function CategoryController(categoryService, $log) {
        var vm = this;

        vm.categories = categoryService.listAllCategories();
        vm.onCategoryClicked = onCategoryClicked;

        function onCategoryClicked(category) {
            $log.info(category);
        }
    }]);