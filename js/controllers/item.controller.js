/**
 * Created by Sviat on 3/15/16.
 */

'use strict';

itemsApp.controller('ItemController', ['$scope', 'itemService', 'categoryService', '$routeParams', '$route', '$log',
    function ($scope, itemService, categoryService, $routeParams, $route, $log) {
        $scope.shouldCreate = $route.current.shouldCreate || false;
        $scope.item = {};
        $scope.item.categories = [];
        $scope.availableCategories = [];

        categoryService.listAllCategories().$promise.then(function (categories) {

            //if we need to show specific item details
            if ($routeParams.itemId) {
                itemService.getById({id: $routeParams.itemId}).$promise.then(function (item) {
                    $scope.item = item;

                    //if item doesn't have any categories - just show all available
                    if (item.categories === undefined) {
                        $scope.availableCategories = categories;
                        return;
                    }

                    //filter categories to show only available to add
                    //todo: better way to filter this!
                    for (var i = 0; i < categories.length; i++) {
                        for (var j = 0; j < item.categories.length; j++) {
                            var contains = false;
                            if (categories[i].id == item.categories[j].id) {
                                contains = true;
                                break;
                            }
                        }

                        if (!contains) {
                            $scope.availableCategories.push(categories[i]);
                        }
                    }
                });
            } else {
                $scope.availableCategories = categories;
            }
        });

        $scope.removeCategory = function (category) {
            for (var i = 0; i < $scope.item.categories.length; i++) {
                if ($scope.item.categories[i].id === category.id) {
                    $scope.item.categories.splice(i, 1);
                    $scope.availableCategories.push(category);
                    break;
                }
            }
        };

        $scope.addCategory = function (category) {
            for (var i = 0; i < $scope.availableCategories.length; i++) {
                if ($scope.availableCategories[i].id === category.id) {
                    $scope.availableCategories.splice(i, 1);
                    $scope.item.categories.push(category);
                    break;
                }
            }
        };

        $scope.createOrUpdateItemClicked = function (item) {
            itemService.saveItem(item);
        };

        $scope.deleteItem = function (item) {
            $log.info('going to delete item with id ' + item.id);
            itemService.deleteItem({id: item.id});
        };
    }]);