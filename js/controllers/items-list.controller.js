/**
 * Created by Sviat on 3/15/16.
 */

'use strict';

itemsApp.controller('ItemsListController', ['$scope', 'itemService', '$log', '$location',
    function ItemsListController($scope, itemService, $log, $location) {
        $scope.hello = 'hello';

        itemService.listAllItems()
            .$promise
            .then(function (items) {
                $scope.items = items;
            })

            .catch(function (response) {
                $log.error(response)
            });

        $scope.getBySingleId = function () {
            console.log(itemService.getById(6));
        };

        $scope.showItemDetails = function (item) {
            $location.url('/details/' + item.id);
        };

        $scope.deleteItem = function (item) {
            itemService.deleteItem({id: item.id}).$promise.then(function (res) {
                $log.info(res);

                for (var i = 0; i < $scope.items.length; i++) {
                    if ($scope.items[i].id === item.id) {
                        $scope.items.splice(i, 1);
                        break;
                    }
                }
            }).catch(function (err) {
                $log.error(err)
            });
        }
    }]);